package raftkv

import (
	"encoding/gob"
	"fmt"
	"labrpc"
	"raft"
	"sync"
	"time"

	"github.com/satori/go.uuid"
)

const ServerTimeout = 20 * time.Second

// Op operation to be performaed on state machine as a result of
// client request
type Op struct {
	// Your definitions here.
	// Field names must start with capital letters,
	// otherwise RPC will break.
	Name  string
	Value interface{}
	// do request needs uuid?? or is Term sufficient?
	UUID string
}

// RaftKV represents KV server
type RaftKV struct {
	mu      sync.Mutex
	me      int
	rf      *raft.Raft
	applyCh chan raft.ApplyMsg

	maxraftstate int // snapshot if log grows this big

	// Your definitions here.
	state map[string]string
	// pending request handlers channel
	pendingReq map[int]chan raft.ApplyMsg
	appliedReq map[int64]int64
}

// StartKVServer initializes KV server
// servers[] contains the ports of the set of
// servers that will cooperate via Raft to
// form the fault-tolerant key/value service.
// me is the index of the current server in servers[].
// the k/v server should store snapshots with persister.SaveSnapshot(),
// and Raft should save its state (including log) with persister.SaveRaftState().
// the k/v server should snapshot when Raft's saved state exceeds maxraftstate bytes,
// in order to allow Raft to garbage-collect its log. if maxraftstate is -1,
// you don't need to snapshot.
// StartKVServer() must return quickly, so it should start goroutines
// for any long-running work.
func StartKVServer(servers []*labrpc.ClientEnd, me int, persister *raft.Persister, maxraftstate int) *RaftKV {
	// call gob.Register on structures you want
	// Go's RPC library to marshall/unmarshall.
	gob.Register(Op{})
	gob.Register(GetArgs{})
	gob.Register(PutAppendArgs{})
	kv := new(RaftKV)
	kv.me = me
	kv.maxraftstate = maxraftstate

	// You may need initialization code here.

	kv.applyCh = make(chan raft.ApplyMsg)
	kv.rf = raft.Make(servers, me, persister, kv.applyCh)
	kv.appliedReq = make(map[int64]int64)

	// You may need initialization code here.
	kv.state = make(map[string]string)
	kv.pendingReq = make(map[int]chan raft.ApplyMsg)
	go kv.Apply()
	return kv
}

// Apply applies the command to state machine
func (kv *RaftKV) Apply() {
	for log := range kv.applyCh {
		DPrintf("KVS-Apply: Received commit message: serverID %v, lobmsg: %v",
			kv.me, log)
		index := log.Index
		command := log.Command.(Op)
		func() {
			kv.mu.Lock()
			defer kv.mu.Unlock()

			// Apply command to state machine
			switch command.Name {
			case "Get":
				// DPrintf("KVSERVER: Received Get Command")
				// args := command.Value.(GetArgs)
				// clientID := args.ClientID
				// reqID := args.ReqID
				// exists := kv.checkDupReq(clientID, reqID)
			case "PutAppend":
				// append to state machine
				// DPrintf("KVSERVER: Received PutAppend Command")
				args := command.Value.(PutAppendArgs)

				if exists := kv.checkDupReq(args.ClientID, args.ReqID); !exists {
					if args.Op == "Put" {
						kv.state[args.Key] = args.Value
					} else if args.Op == "Append" {
						if _, exists := kv.state[args.Key]; !exists {
							kv.state[args.Key] = ""
						}
						kv.state[args.Key] += args.Value
						DPrintf("KVS-Applied-Result: server-id: %v key: %v append-value: %v", kv.me, args.Key, kv.state[args.Key])
					}
					kv.updateApplied(args.ClientID, args.ReqID)
				} else {
					DPrintf("KVS-Apply-Duplicate message: serverID: %v, logmsg: %v",
						kv.me, log)
				}
			default:
				panic(fmt.Sprintf("Command faulty: %v, name: %v", command, command.Name))
			}

			// Check if there is any pending request.
			if waitCh, exists := kv.pendingReq[index]; exists {
				DPrintf("KVS Chk Is Waiting on: serverId: %v, index: %v",
					kv.me, index)
				waitCh <- log
			}

		}()
	}
}

// Should call this method with lock over kv
func (kv *RaftKV) checkDupReq(clientID, reqID int64) bool {
	if prevReqID, ok := kv.appliedReq[clientID]; ok {
		if prevReqID == reqID {
			DPrintf("KVS-Req-Already Exists: serverID: %v, clientID:%v, reqID: %v",
				kv.me, clientID, reqID)
			return true
		}
	}
	return false
}

func (kv *RaftKV) updateApplied(clientID, reqID int64) {
	kv.appliedReq[clientID] = reqID
}

// Get handler
func (kv *RaftKV) Get(args *GetArgs, reply *GetReply) {
	// Your code here.
	uuid := uuid.NewV4().String()
	command := Op{
		Name:  "Get",
		Value: *args,
		UUID:  uuid,
	}
	index, _, isLeader := kv.rf.Start(command)
	if isLeader {
		ch := make(chan raft.ApplyMsg)
		func() {
			// create wait channel and read from it.
			kv.mu.Lock()
			defer kv.mu.Unlock()
			kv.pendingReq[index] = ch

		}()

		// keeping client wait till we hear response on waitCh
		DPrintf("KVS-GET-LEADER-WAITING: serverID: %v, index: %v, cmd:%v, args: %v",
			kv.me, index, command, command.Value)

		select {
		case <-time.After(ServerTimeout):
			DPrintf("KVS-GET-LEADER-Timeout: serverID: %v, index: %v, cmd:%v, args: %v",
				kv.me, index, command, command.Value)
			func() {
				kv.mu.Lock()
				defer kv.mu.Unlock()
				reply.Err = "Leader Timedout - Because of Parition"
				reply.WrongLeader = true
				reply.Value = ""
				delete(kv.pendingReq, index)
			}()

			return
		case logmsg := <-ch:
			DPrintf("KVS-GET-LEADER-WAITING-Done: serverID: %v, index: %v, cmd:%v, args: %v",
				kv.me, index, command, command.Value)
			kv.mu.Lock()
			defer kv.mu.Unlock()
			rcvdCmd := logmsg.Command.(Op)
			if rcvdCmd.UUID == uuid {
				// Got Same Request i.e log got committed
				reply.Err = "Success"
				reply.WrongLeader = false
				reply.Value = kv.state[args.Key]
				DPrintf("KVRAFT-SERVER-Get-Suc: serverID: %v, index: %v, command:%v, args: %v, reply:%v",
					kv.me, index, command, command.Value, reply)
			} else {
				// Ask client to retry as log did not get committed.

				reply.Err = "Leader changed before log got committed"
				reply.WrongLeader = false
				reply.Value = ""
				DPrintf("KVRAFT-SERVER-Get-Fail: serverID: %v, index: %v, exp_cmd:%v, exp_arg: %v, act_cmd: %v, act_args: %v, reply:%v",
					kv.me, index, command, command.Value, rcvdCmd, rcvdCmd.Value, reply)
			}
			delete(kv.pendingReq, index)
			kv.updateApplied(args.ClientID, args.ReqID)
		}

	} else {
		reply.Err = "Not Leader"
		reply.WrongLeader = true
		reply.Value = ""
		DPrintf("KVS-Get-Not Leader: serverID: %v, index: %v, cmd:%v, args: %v, reply:%v",
			kv.me, index, command, command.Value, reply)
	}
}

// PutAppend handler
func (kv *RaftKV) PutAppend(args *PutAppendArgs, reply *PutAppendReply) {
	uuid := uuid.NewV4().String()
	command := Op{
		Name:  "PutAppend",
		Value: *args,
		UUID:  uuid,
	}
	index, _, isLeader := kv.rf.Start(command)
	if isLeader {
		ch := make(chan raft.ApplyMsg)
		func() {
			// create wait channel and read from it.
			kv.mu.Lock()
			defer kv.mu.Unlock()
			DPrintf("KVS-%v-Leader-created channel for command: %v, serverID: %v", args.Op, command, kv.me)
			kv.pendingReq[index] = ch
		}()

		// keeping client wait till we hear response on waitCh
		DPrintf("KVS-%v-LEADER-WAITING:: serverID: %v, index: %v, cmd:%v, args: %v",
			args.Op, kv.me, index, command, command.Value)

		select {
		case <-time.After(ServerTimeout):
			func() {
				DPrintf("KVS-%v-LEADER-Timedout:: serverID: %v, index: %v, cmd:%v, args: %v",
					args.Op, kv.me, index, command, command.Value)
				kv.mu.Lock()
				defer kv.mu.Unlock()
				reply.Err = "Leader Timedout - Because of Parition"
				reply.WrongLeader = true
				delete(kv.pendingReq, index)
			}()
			return
		case logmsg := <-ch:
			DPrintf("KVS-%v-LEADER-WAITING-Done:: serverID: %v, index: %v, cmd:%v, args: %v",
				args.Op, kv.me, index, command, command.Value)
			kv.mu.Lock()
			defer kv.mu.Unlock()
			rcvdCmd := logmsg.Command.(Op)
			if rcvdCmd.UUID == uuid {
				// Got Same Request i.e log got committed
				reply.Err = "Success"
				reply.WrongLeader = false
				DPrintf("KVRAFT-SERVER-%v-Suc: serverID: %v, index: %v, command:%v, args: %v, reply:%v ",
					args.Op, kv.me, index, command, command.Value, reply)
			} else {
				// Ask client to retry as log did not get committed.

				reply.Err = "Leader changed before log got committed"
				reply.WrongLeader = true
				DPrintf("KVRAFT-SERVER-%v-Fail: serverID: %v, index: %v, exp_cmd:%v, exp_arg: %v, act_cmd: %v, act_args: %v, reply:%v",
					args.Op, kv.me, index, command, command.Value, rcvdCmd, rcvdCmd.Value, reply)
			}
			delete(kv.pendingReq, index)
		}

		// DPrintf("KVRAFT-SERVER-%v-Suc-Ret: serverID: %v, index: %v, command:%v, args: %v, reply:%v ",
		// 	args.Op, kv.me, index, command, command.Value, reply)
	} else {
		reply.Err = "Not Leader"
		reply.WrongLeader = true
		DPrintf("KVS-%v-Not-Leader: serverID: %v, index: %v, cmd:%v, args: %v, reply:%v",
			args.Op, kv.me, index, command, command.Value, reply)
	}
}

//
// the tester calls Kill() when a RaftKV instance won't
// be needed again. you are not required to do anything
// in Kill(), but it might be convenient to (for example)
// turn off debug output from this instance.
//
func (kv *RaftKV) Kill() {
	kv.rf.Kill()
	// Your code here, if desired.
}
