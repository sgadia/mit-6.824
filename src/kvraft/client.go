package raftkv

import "labrpc"
import "crypto/rand"
import "math/big"
import "fmt"

type Clerk struct {
	servers []*labrpc.ClientEnd
	// You will have to modify this struct.
	lastLeaderID int
	me           int64
}

func nrand() int64 {
	max := big.NewInt(int64(1) << 62)
	bigx, _ := rand.Int(rand.Reader, max)
	x := bigx.Int64()
	return x
}

func MakeClerk(servers []*labrpc.ClientEnd) *Clerk {
	ck := new(Clerk)
	ck.servers = servers
	// You'll have to add code here.
	ck.me = nrand()

	return ck
}

// Put command
func (ck *Clerk) Put(key string, value string) {
	ck.PutAppend(key, value, "Put")
}

// Append command
func (ck *Clerk) Append(key string, value string) {
	ck.PutAppend(key, value, "Append")
}

// Get command - fetch the current value for a key.
// returns "" if the key does not exist.
// keeps trying forever in the face of all other errors.
//
// you can send an RPC with code like this:
// ok := ck.servers[i].Call("RaftKV.Get", &args, &reply)
//
// the types of args and reply (including whether they are pointers)
// must match the declared types of the RPC handler function's
// arguments. and reply must be passed as a pointer.
func (ck *Clerk) Get(key string) string {
	DPrintf("KVC(%v): Initiating Get command: key: %v", ck.me, key)
	args := GetArgs{
		Key:      key,
		ReqID:    nrand(),
		ClientID: ck.me,
	}
	totalServers := len(ck.servers)

	if totalServers > 0 {
		i := ck.lastLeaderID
		for {
			i = i % totalServers
			reply := &GetReply{}
			if ok := ck.servers[i].Call("RaftKV.Get", &args, reply); !ok {
				DPrintf("KVC(%v)-Get-Timeout serverID: %v, args: %v, reply: %v", ck.me, i, args, reply)
				i++
				continue
			}
			DPrintf("KVC(%v)-GET-Response: serverID: %v, args: %v, reply:%v", ck.me, i, args, reply)
			if reply.WrongLeader {
				i++
			} else {
				if reply.Err == "Success" {
					ck.lastLeaderID = i
					return reply.Value
				}
				// Print Error -> continue or bubble up??
				// Should we retry indefinitely??
				// Should we retry on same server or next one??
				i++

			}

		}
	} else {
		panic(fmt.Sprintf("KVC(%v)-PUTAPPEND: No servers available", ck.me))
	}
}

// PutAppend shared by Put and Append.
//
// you can send an RPC with code like this:
// ok := ck.servers[i].Call("RaftKV.PutAppend", &args, &reply)
//
// the types of args and reply (including whether they are pointers)
// must match the declared types of the RPC handler function's
// arguments. and reply must be passed as a pointer.
//
func (ck *Clerk) PutAppend(key string, value string, op string) {
	// You will have to modify this function.
	DPrintf("KVC(%v): Initiating PutAppend command: key: %v, value: %v, op: %v",
		ck.me, key, value, op)
	args := PutAppendArgs{
		Key:      key,
		Value:    value,
		Op:       op,
		ReqID:    nrand(),
		ClientID: ck.me,
	}
	totalServers := len(ck.servers)

	if totalServers > 0 {
		i := ck.lastLeaderID
		for {
			i = i % totalServers
			reply := &PutAppendReply{}
			if ok := ck.servers[i].Call("RaftKV.PutAppend", &args, reply); !ok {
				DPrintf("KVC(%v)-PutAppend-Timeout serverID: %v, args: %v, reply: %v", ck.me, i, args, reply)
				i++
				continue
			}
			DPrintf("KVC(%v)-PutAppend-Response: serverID: %v, args: %v, reply:%v", ck.me, i, args, reply)
			if reply.WrongLeader {
				i++
			} else {
				if reply.Err == "Success" {
					ck.lastLeaderID = i
					break
				} else {
					// Print Error -> continue or bubble up??
					// Should we retry indefinitely??
					// Should we retry on same server or next one??
					i++

				}
			}
		}
	} else {
		panic(fmt.Sprintf("KVC(%v)-PUTAPPEND: No servers available", ck.me))
	}

}
