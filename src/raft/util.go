package raft

import "log"

// Debug enables debug log
const Debug = 0

// DPrintf prints raft debug log
func DPrintf(format string, a ...interface{}) (n int, err error) {
	if Debug > 0 {
		log.Printf(format, a...)
	}
	return
}
