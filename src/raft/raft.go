package raft

//
// this is an outline of the API that raft must expose to
// the service (or tester). see comments below for
// each of these functions for more details.
//
// rf = Make(...)
//   create a new Raft server.
// rf.Start(command interface{}) (index, term, isleader)
//   start agreement on a new log entry
// rf.GetState() (term, isLeader)
//   ask a Raft for its current term, and whether it thinks it is leader
// ApplyMsg
//   each time a new entry is committed to the log, each Raft peer
//   should send an ApplyMsg to the service (or tester)
//   in the same server.
//

import (
	"bytes"
	"encoding/gob"
	"labrpc"
	"math/rand"
	"sort"
	"sync"
	"time"
)

// Timeout constant used throughout raft
const (
	// HeartbeatTimeout is interval leader messages every server in cluster
	HeartbeatTimeout = 150 * time.Millisecond // 170 +/- 50
	// HeartbeatDelta is to avoid split brain and other network partition problems
	HeartbeatDelta int32 = 50
	// ElectionTimeout is interval that every follower waits before transitioning to
	// candidate state
	ElectionTimeout = 475 * time.Millisecond // 475 +/- 100
	// ElectionDelta is to avoid split brain and other network partition problems
	ElectionDelta = 50
)

// server states
const (
	// LeaderState represents the server is leader in cluster
	LeaderState = "leader"
	// CandidateState represents the server is candidate state
	CandidateState = "candidate"
	// FollowerState represents the server as in follower state in cluster
	FollowerState = "follower"
)

const (
	// NotVoted indicates that the server has not casted vote for
	// its currentTerm
	NotVoted = -1
	// NullTerm is nil value for Term
	NullTerm = 0
)

// import "bytes"
// import "encoding/gob"

// ApplyMsg - as each Raft peer becomes aware that successive log entries are
// committed, the peer should send an ApplyMsg to the service (or
// tester) on the same server, via the applyCh passed to Make().
type ApplyMsg struct {
	Index       int
	Command     interface{}
	UseSnapshot bool   // ignore for lab2; only used in lab3
	Snapshot    []byte // ignore for lab2; only used in lab3
}

// LogEntry is single log message
type LogEntry struct {
	Index   int
	Term    int
	Command interface{}
}

// Raft - A Go object implementing a single Raft peer.
type Raft struct {
	mu        sync.Mutex          // Lock to protect shared access to this peer's state
	peers     []*labrpc.ClientEnd // RPC end points of all peers
	persister *Persister          // Object to hold this peer's persisted state
	me        int                 // this peer's index into peers[]

	// Your data here (2A, 2B, 2C).
	// Look at the paper's Figure 2 for a description of what
	// state a Raft server must maintain.
	applyMsg     chan ApplyMsg
	currentTerm  int
	currentState string
	startTime    int64
	votedFor     int
	log          []LogEntry
	commitIndex  int
	lastApplied  int

	// only relevant to leader
	nextIndex  []int
	matchIndex []int
}

// GetState returns currentTerm and whether this server
// believes it is the leader.
func (rf *Raft) GetState() (term int, isleader bool) {
	rf.mu.Lock()
	defer rf.mu.Unlock()
	term = rf.currentTerm
	isleader = rf.currentState == LeaderState
	return
}

// persit saves Raft's persistent state to stable storage,
// where it can later be retrieved after a crash and restart.
// see paper's Figure 2 for a description of what should be persistent.
func (rf *Raft) persist() {
	w := new(bytes.Buffer)
	e := gob.NewEncoder(w)
	e.Encode(rf.currentTerm)
	e.Encode(rf.votedFor)
	e.Encode(rf.log)
	data := w.Bytes()
	rf.persister.SaveRaftState(data)
	DPrintf("Persisted state for Server: %v, term: %v, votedFor: %v, log: %v", rf.me, rf.currentTerm, rf.votedFor, rf.log)
}

// readPersist restores previously persisted state.
func (rf *Raft) readPersist(data []byte) {
	if data == nil || len(data) < 1 {
		// bootstrap without any state?
		return
	}
	r := bytes.NewBuffer(data)
	d := gob.NewDecoder(r)
	d.Decode(&rf.currentTerm)
	d.Decode(&rf.votedFor)
	d.Decode(&rf.log)
}

// RequestVoteArgs is RequestVote RPC arguments structure.
// field names must start with capital letters!
type RequestVoteArgs struct {
	Term        int
	CandidateID int
	LastLogID   int
	LastLogTerm int
}

// RequestVoteReply is RequestVote RPC reply structure.
// field names must start with capital letters!
type RequestVoteReply struct {
	Term        int
	VoteGranted bool
}

func createRequestArgs(rf *Raft) (args RequestVoteArgs) {
	args.Term = rf.currentTerm
	args.CandidateID = rf.me
	args.LastLogID = len(rf.log)
	if args.LastLogID > 0 {
		args.LastLogTerm = rf.log[args.LastLogID-1].Term
	}
	return
}

// example code to send a RequestVote RPC to a server.
// server is the index of the target server in rf.peers[].
// expects RPC arguments in args.
// fills in *reply with RPC reply, so caller should
// pass &reply.
// the types of the args and reply passed to Call() must be
// the same as the types of the arguments declared in the
// handler function (including whether they are pointers).
//
// The labrpc package simulates a lossy network, in which servers
// may be unreachable, and in which requests and replies may be lost.
// Call() sends a request and waits for a reply. If a reply arrives
// within a timeout interval, Call() returns true; otherwise
// Call() returns false. Thus Call() may not return for a while.
// A false return can be caused by a dead server, a live server that
// can't be reached, a lost request, or a lost reply.
//
// Call() is guaranteed to return (perhaps after a delay) *except* if the
// handler function on the server side does not return.  Thus there
// is no need to implement your own timeouts around Call().
//
// look at the comments in ../labrpc/labrpc.go for more details.
//
// if you're having trouble getting RPC to work, check that you've
// capitalized all field names in structs passed over RPC, and
// that the caller passes the address of the reply struct with &, not
// the struct itself.
func (rf *Raft) sendRequestVote(server int, args *RequestVoteArgs, reply *RequestVoteReply) bool {
	return rf.peers[server].Call("Raft.RequestVote", args, reply)
}

func contestElection(rf *Raft, args RequestVoteArgs, startTime int64) {
	totalVotes := 1
	reqVoteCount := len(rf.peers)/2 + 1
	DPrintf("Candidate: %v - starting election.", rf.me)
	// send RequestVote RPC to all other peers concurrently.
	for i := 0; i < len(rf.peers); i++ {
		if i == rf.me {
			continue
		}
		go func(serverID int) {
			reply := &RequestVoteReply{}
			if ok := rf.sendRequestVote(serverID, &args, reply); !ok {
				DPrintf("Req.Vote RPC Failed. Receiver: %v, Args = [CID: %v, Term: %v, LLID: %v, LLT: %v]",
					serverID, args.CandidateID, args.Term, args.LastLogID, args.LastLogTerm)
				return
			}
			DPrintf("Contest Election Response from SID: %v - args=[CID :%v, Term: %v, LLID: %v, LLT: %v], reply=[ID: %v, Term: %v, Granted: %v]",
				serverID, args.CandidateID, args.Term, args.LastLogID, args.LastLogTerm, serverID, reply.Term, reply.VoteGranted)
			rf.mu.Lock()
			defer rf.mu.Unlock()
			if rf.currentTerm == args.Term && rf.startTime == startTime {
				// server is still in same state
				if reply.VoteGranted {
					totalVotes++
					if totalVotes >= reqVoteCount {
						resetServer(rf, rf.currentTerm, LeaderState, rf.me)
						go becomeLeader(rf, rf.currentTerm, rf.startTime)
						return
					}
				} else {
					if reply.Term > rf.currentTerm {
						resetServer(rf, reply.Term, FollowerState, NotVoted)
						go becomeFollower(rf, rf.currentTerm, rf.startTime)
						return
					}
					// Log of this candidate is not as up-to-date as ServerID
					DPrintf("Vote not granted because of log not being up to date")
				}
			}

		}(i)
	}

}

// RequestVote RPC handler.
func (rf *Raft) RequestVote(args *RequestVoteArgs, reply *RequestVoteReply) {
	rf.mu.Lock()
	defer rf.mu.Unlock()
	defer rf.persist()
	DPrintf("RequestVoteRPC Started - Receiver: [ID: %v, Term: %v, state: %v], Candidate:%v args:[Term: %v, CandidateID: %v, LastLogID: %v, LastLogTerm: %v]",
		rf.me, rf.currentTerm, rf.currentState, args.CandidateID, args.Term, args.CandidateID, args.LastLogID, args.LastLogTerm)
	restart := false
	reply.Term = rf.currentTerm
	reply.VoteGranted = false
	if args.Term < rf.currentTerm {
		return
	}
	if args.Term > rf.currentTerm {
		resetServer(rf, args.Term, FollowerState, NotVoted)
		restart = true
	}
	if rf.votedFor == args.CandidateID || rf.votedFor == NotVoted {
		// validate if the logs are up-to-date
		prevLogID := len(rf.log)
		prevLogTerm := 0
		if prevLogID > 0 {
			prevLogTerm = rf.log[prevLogID-1].Term
		}
		if args.LastLogTerm > prevLogTerm {
			reply.VoteGranted = true
			restart = true
		} else if args.LastLogTerm == prevLogTerm {
			if args.LastLogID >= prevLogID {
				reply.VoteGranted = true
				restart = true
			}
		}
	}
	if reply.VoteGranted {
		resetServer(rf, args.Term, FollowerState, args.CandidateID)
	}
	if restart {
		go becomeFollower(rf, rf.currentTerm, rf.startTime)
	}
	reply.Term = rf.currentTerm
	DPrintf("RequestVoteRPC Ended - Receiver: [ID: %v, Term: %v, state: %v], Candidate:%v args:[Term: %v, CandidateID: %v, LastLogID: %v, LastLogTerm: %v], Reply: [Term: %v, Granted: %v]",
		rf.me, rf.currentTerm, rf.currentState, args.CandidateID, args.Term, args.CandidateID, args.LastLogID, args.LastLogTerm, reply.Term, reply.VoteGranted)
}

// AppendEntriesArgs is AppendEntries RPC argument structure
// sent by cluster leader
type AppendEntriesArgs struct {
	Term           int
	LeaderID       int
	PrevLogID      int
	PrevLogTerm    int
	Entries        []LogEntry
	LeaderCommitID int
}

// AppendEntriesReply is reply structure returned by
// AppendEntries RPC handler
type AppendEntriesReply struct {
	Term          int
	Success       bool
	ConflictTerm  int
	ConflictIndex int
}

func triggerAppendEntries(rf *Raft, serverID int, term int, startTime int64) {
	quit := false
	var args AppendEntriesArgs
	for {
		func() {
			rf.mu.Lock()
			defer rf.mu.Unlock()
			if term == rf.currentTerm && startTime == rf.startTime {
				args = createAppendEntriesArgs(rf, serverID)
			} else {
				quit = true
			}
		}()
		if quit {
			return
		}
		reply := &AppendEntriesReply{}
		if ok := rf.sendAppendEntries(serverID, &args, reply); !ok {
			// RPC failed. Resend
			continue
		}
		// RPC response arrived
		func() {
			rf.mu.Lock()
			defer rf.mu.Unlock()
			quit = true
			if term == rf.currentTerm && startTime == rf.startTime {
				if reply.Success {
					// Guard against outdated Responses.
					if (args.PrevLogID + len(args.Entries) + 1) > rf.nextIndex[serverID] {
						rf.nextIndex[serverID] = args.PrevLogID + len(args.Entries) + 1
					}
					rf.matchIndex[serverID] = rf.nextIndex[serverID] - 1
					DPrintf("Successfull AppendRPC Reply - From Server: %v - Arguments:[Term: %v, LID: %v, PLID: %v, PLT:%v, LCMID: %v, Entries: %v], NIDX: %v, MIDX: %v",
						serverID, args.Term, args.LeaderID, args.PrevLogID, args.PrevLogTerm, args.LeaderCommitID, args.Entries, rf.nextIndex, rf.matchIndex)
					// Update Commit Index
					func() {
						matchSlice := make([]int, len(rf.peers)-1)
						idx := 0
						for i := 0; i < len(rf.peers); i++ {
							if i == rf.me {
								continue
							}
							matchSlice[idx] = rf.matchIndex[i]
							idx++
						}
						sort.Ints(matchSlice)
						majorityID := len(matchSlice) / 2

						// Guarding against updating commit index to log entry not matching currentTerm.
						if matchSlice[majorityID] > 0 && rf.currentTerm == rf.log[matchSlice[majorityID]-1].Term {
							if matchSlice[majorityID] > rf.commitIndex {
								rf.commitIndex = matchSlice[majorityID]
								DPrintf("UPDATED COMMITIDX - LID: %v, Term: %v, CMID: %v", rf.me, rf.currentTerm, rf.commitIndex)
							}
						}
					}()
				} else {
					DPrintf("Unsuccessfull AppendRPC Reply - From Server: %v - Arguments:[Term: %v, LID: %v, PLID: %v, PLT:%v, LCMID: %v, Entries: %v], NIDX: %v, MIDX: %v, reply: [Term: %v, Success: %v]",
						serverID, args.Term, args.LeaderID, args.PrevLogID, args.PrevLogTerm, args.LeaderCommitID, args.Entries, rf.nextIndex, rf.matchIndex, reply.Term, reply.Success)
					if reply.Term > rf.currentTerm {
						// Become follower
						resetServer(rf, reply.Term, FollowerState, NotVoted)
						go becomeFollower(rf, rf.currentTerm, rf.startTime)
						return
					}

					// log entry at args.PrevLogID did not match
					if rf.nextIndex[serverID] > 1 {
						// rf.nextIndex[serverID]--
						if reply.ConflictTerm == 0 {
							rf.nextIndex[serverID] = reply.ConflictIndex
						} else {
							// search for conflict term
							startIDX := -1
							for i := 1; i <= len(rf.log) && rf.log[i-1].Term < reply.ConflictTerm; i++ {
								if rf.log[i-1].Term == reply.ConflictTerm {
									startIDX = i
									break
								}
							}
							if startIDX == -1 {
								rf.nextIndex[serverID] = reply.ConflictIndex
							} else {
								found := -1
								for i := startIDX; i <= len(rf.log); i++ {
									if rf.log[i-1].Term != reply.ConflictTerm {
										found = i
										break
									}
								}
								if found == -1 {
									rf.nextIndex[serverID] = reply.ConflictIndex
								} else {
									rf.nextIndex[serverID] = found
								}
							}
						}
					}
					// resend on log mismatch
					quit = false
				}
			}
		}()

		if quit {
			return
		}

	}
}

// should be used when we have lock over raft server
func createAppendEntriesArgs(rf *Raft, serverID int) (arg AppendEntriesArgs) {
	arg.Term = rf.currentTerm
	arg.LeaderID = rf.me
	if rf.nextIndex[serverID] > 1 {
		arg.PrevLogID = rf.nextIndex[serverID] - 1
		arg.PrevLogTerm = rf.log[arg.PrevLogID-1].Term
	}
	arg.LeaderCommitID = rf.commitIndex
	arg.Entries = append(arg.Entries, rf.log[arg.PrevLogID:]...)
	return
}

// sendAppendEntries is AppendEntries RPC call.
func (rf *Raft) sendAppendEntries(server int, args *AppendEntriesArgs, reply *AppendEntriesReply) bool {
	return rf.peers[server].Call("Raft.AppendEntries", args, reply)
}

// AppendEntries RPC handler
func (rf *Raft) AppendEntries(args *AppendEntriesArgs, reply *AppendEntriesReply) {
	rf.mu.Lock()
	defer rf.mu.Unlock()
	defer rf.persist()
	DPrintf("AppendEntriesRPC - Receiver [ID: %v, Term: %v, State: %v], Leader: [ID: %v, Term: %v, PrevLogID: %v, PrevLogTerm: %v, Entries: %v, CommitID: %v]",
		rf.me, rf.currentTerm, rf.currentState, args.LeaderID, args.Term, args.PrevLogID, args.PrevLogTerm, args.Entries, args.LeaderCommitID)
	restart := false
	reply.Success = false
	reply.Term = rf.currentTerm

	// Restart as follower if required at the end of RPC
	defer func() {
		if restart {
			go becomeFollower(rf, rf.currentTerm, rf.startTime)
		}
	}()

	if args.Term < rf.currentTerm {
		return
	} else if args.Term == rf.currentTerm {
		// Valid leader as only one leader can get elected for
		// a given term
		resetServer(rf, args.Term, FollowerState, rf.votedFor)
		restart = true
	} else {
		resetServer(rf, args.Term, FollowerState, NotVoted)
		restart = true
	}

	// Log matching and update property
	if args.PrevLogID <= len(rf.log) {
		if args.PrevLogID == 0 || args.PrevLogTerm == rf.log[args.PrevLogID-1].Term {
			reply.Success = true
			totalEntries := len(args.Entries)
			if (args.PrevLogID + totalEntries) > rf.commitIndex {
				if (args.PrevLogID + totalEntries) > len(rf.log) {
					rf.log = append(rf.log[:args.PrevLogID], args.Entries...)
				} else {
					for i := 0; i < len(args.Entries); i++ {
						rf.log[args.PrevLogID+i] = args.Entries[i]
					}
				}
				if rf.commitIndex < args.LeaderCommitID {
					if (args.PrevLogID + totalEntries) < args.LeaderCommitID {
						rf.commitIndex = args.PrevLogID + totalEntries
					} else {
						rf.commitIndex = args.LeaderCommitID
					}
				}
			}

			DPrintf("Successful AppendEntries - Server: %v, Term: %v, State: %v, CommitID: %v, Log: %v",
				rf.me, rf.currentTerm, rf.currentState, rf.commitIndex, rf.log)
		} else {
			// Term mismatch optimization for log backing
			reply.ConflictTerm = rf.log[args.PrevLogID-1].Term
			reply.ConflictIndex = 1
			for i := args.PrevLogID - 1; i > 0; i-- {
				if rf.log[i-1].Term != reply.ConflictTerm {
					reply.ConflictIndex = i + 1
					break
				}
			}
			rf.log = rf.log[:reply.ConflictIndex]
			DPrintf("Unsuccessful AppendEntries (Log Mismatch Case) - Server: %v, Term: %v, State: %v, CommitID: %v, Log: %v",
				rf.me, rf.currentTerm, rf.currentState, rf.commitIndex, rf.log)
		}
	} else {
		// log backing Optimization when server log is less
		// than leader appendEntries Prevous log ID.
		reply.ConflictIndex = len(rf.log)
		reply.ConflictTerm = 0
	}

}

// Start communication - the service using Raft (e.g. a k/v server) wants to start
// agreement on the next command to be appended to Raft's log. if this
// server isn't the leader, returns false. otherwise start the
// agreement and return immediately. there is no guarantee that this
// command will ever be committed to the Raft log, since the leader
// may fail or lose an election.
//
// the first return value is the index that the command will appear at
// if it's ever committed. the second return value is the current
// term. the third return value is true if this server believes it is
// the leader.
func (rf *Raft) Start(command interface{}) (index int, term int, isLeader bool) {
	index = -1
	term = -1
	isLeader = true
	// Your code here (2B).
	rf.mu.Lock()
	defer rf.mu.Unlock()
	if rf.currentState != LeaderState {
		isLeader = false
		return
	}
	index = len(rf.log) + 1
	term = rf.currentTerm
	rf.log = append(rf.log, LogEntry{
		Index:   index,
		Term:    term,
		Command: command,
	})
	rf.persist()
	rf.nextIndex[rf.me] = index + 1
	rf.matchIndex[rf.me] = index
	DPrintf("NEW COMMAND RCVD: [ID: %v, TERM: %v, IDX: %v, command: %v]", rf.me, rf.currentTerm, index, command)
	return
}

// Kill - the tester calls Kill() when a Raft instance won't
// be needed again. you are not required to do anything
// in Kill(), but it might be convenient to (for example)
// turn off debug output from this instance.
func (rf *Raft) Kill() {
	// Debug = 0
}

func becomeFollower(rf *Raft, term int, startTime int64) {
	DPrintf("Follower State: server:%v, term:%v, startTime:%v", rf.me, term, startTime)
	timer := ElectionTimeout + time.Duration(rand.Int31n(ElectionDelta))*time.Millisecond
	time.Sleep(timer)
	rf.mu.Lock()
	defer rf.mu.Unlock()
	if rf.currentTerm == term && rf.startTime == startTime {
		// change to candidate
		rf.currentTerm++
		rf.votedFor = rf.me
		rf.persist()
		rf.currentState = CandidateState
		rf.startTime = time.Now().UnixNano()
		args := createRequestArgs(rf)
		go becomeCandidate(rf, rf.currentTerm, rf.startTime, args)
	}
}

func becomeCandidate(rf *Raft, term int, startTime int64, args RequestVoteArgs) {
	DPrintf("Candidate State: server:%v, term:%v, startTime:%v, args:%v", rf.me, term, startTime, args)
	go contestElection(rf, args, startTime)
	timer := ElectionTimeout + time.Duration(rand.Int31n(ElectionDelta))*time.Millisecond
	time.Sleep(timer)
	rf.mu.Lock()
	defer rf.mu.Unlock()
	if rf.currentTerm == term && rf.startTime == startTime {
		// Restart the election.
		rf.currentTerm++
		rf.votedFor = rf.me
		rf.persist()
		rf.currentState = CandidateState
		rf.startTime = time.Now().UnixNano()
		go becomeCandidate(rf, rf.currentTerm, rf.startTime, createRequestArgs(rf))
	}
}

func becomeLeader(rf *Raft, term int, startTime int64) {
	DPrintf("Leader State: Server: %v, term: %v, startTime: %v", rf.me, term, startTime)
	quit := false
	for {
		func() {
			rf.mu.Lock()
			defer rf.mu.Unlock()
			if rf.currentTerm == term && rf.startTime == startTime {
				// DPrintf("APPENDRPC - HEARTBEAT CYCLE")
				for i := 0; i < len(rf.peers); i++ {
					if i == rf.me {
						continue
					}
					func(serverID int) {
						go triggerAppendEntries(rf, serverID, term, startTime)
					}(i)

				}
			} else {
				quit = true
				return
			}
		}()
		if quit {
			return
		}
		// IMP IMP IMP [Election Timeout >> Heartbeat >> agg. response time from all servers]
		timer := HeartbeatTimeout + time.Duration(rand.Int31n(HeartbeatDelta))*time.Millisecond
		time.Sleep(timer)
	}
}

// Make - the service or tester wants to create a Raft server. the ports
// of all the Raft servers (including this one) are in peers[]. this
// server's port is peers[me]. all the servers' peers[] arrays
// have the same order. persister is a place for this server to
// save its persistent state, and also initially holds the most
// recent saved state, if any. applyCh is a channel on which the
// tester or service expects Raft to send ApplyMsg messages.
// Make() must return quickly, so it should start goroutines
// for any long-running work.
//
func Make(peers []*labrpc.ClientEnd, me int,
	persister *Persister, applyCh chan ApplyMsg) *Raft {
	rf := &Raft{}
	rf.peers = peers
	rf.persister = persister
	rf.me = me
	rf.applyMsg = applyCh
	rf.currentTerm = 0
	rf.votedFor = NotVoted
	rf.currentState = FollowerState
	rf.log = make([]LogEntry, 0)
	rf.commitIndex = 0
	rf.lastApplied = 0
	rf.nextIndex = make([]int, len(rf.peers))
	rf.matchIndex = make([]int, len(rf.peers))

	// initialize from state persisted before a crash
	rf.readPersist(persister.ReadRaftState())
	go rf.applyLogToState()
	go becomeFollower(rf, rf.currentTerm, rf.startTime)
	return rf
}

// resetServer resets raft state. Caller should have the lock over raft
func resetServer(rf *Raft, term int, state string, votedFor int) {
	rf.currentTerm = term
	rf.currentState = state
	rf.votedFor = votedFor
	rf.persist()
	rf.startTime = time.Now().UnixNano()
	if state == LeaderState {
		llen := len(rf.log)
		for i := 0; i < len(rf.peers); i++ {
			rf.nextIndex[i] = llen + 1
			if i == rf.me {
				rf.matchIndex[i] = llen
			} else {
				rf.matchIndex[i] = rf.commitIndex
			}
		}
	}
}

// applyLogToState applies committed logs to server's state machine
func (rf *Raft) applyLogToState() {
	for {
		rf.mu.Lock()
		if rf.lastApplied < rf.commitIndex {
			DPrintf("APPLYING LOG - server: %v, term: %v, state: %v, LA: %v, CMID: %v, Log: %v",
				rf.me, rf.currentTerm, rf.currentState, rf.lastApplied, rf.commitIndex, rf.log)
			rf.applyMsg <- ApplyMsg{
				Index:   rf.log[rf.lastApplied].Index,
				Command: rf.log[rf.lastApplied].Command,
			}
			rf.lastApplied++
		}
		rf.mu.Unlock()
		time.Sleep(10 * time.Millisecond)
	}
}
