package mapreduce

import (
	"fmt"
	"sync"
)

//
// schedule() starts and waits for all tasks in the given phase (Map
// or Reduce). the mapFiles argument holds the names of the files that
// are the inputs to the map phase, one per map task. nReduce is the
// number of reduce tasks. the registerChan argument yields a stream
// of registered workers; each item is the worker's RPC address,
// suitable for passing to call(). registerChan will yield all
// existing registered workers (if any) and new ones as they register.
//
func schedule(jobName string, mapFiles []string, nReduce int, phase jobPhase, registerChan chan string) {
	var ntasks int
	var n_other int // number of inputs (for reduce) or outputs (for map)
	switch phase {
	case mapPhase:
		ntasks = len(mapFiles)
		n_other = nReduce
	case reducePhase:
		ntasks = nReduce
		n_other = len(mapFiles)
	}

	fmt.Printf("Schedule: %v %v tasks (%d I/Os)\n", ntasks, phase, n_other)

	// All ntasks tasks have to be scheduled on workers, and only once all of
	// them have been completed successfully should the function return.
	// Remember that workers may fail, and that any given worker may finish
	// multiple tasks.
	//
	// TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
	//
	var completedTask int
	var m sync.Mutex
	tasks := make(chan int, 20)
	done := make(chan struct{})

	var wg sync.WaitGroup

	taskDone := func() {
		m.Lock()
		defer m.Unlock()
		completedTask++
		if completedTask == ntasks {
			done <- struct{}{}
		}
	}

	// Initial task feeder
	wg.Add(1)
	go func(totalTasks int) {
		defer wg.Done()
		for i := 0; i < totalTasks; i++ {
			tasks <- i
		}
	}(ntasks)

	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case <-done:
				close(tasks)
				return
			case taskID := <-tasks:
				waddr := <-registerChan
				wg.Add(1)
				go func(workerAddr string, tid int) {
					file := ""
					if phase == mapPhase {
						file = mapFiles[tid]
					}
					success := call(workerAddr, "Worker.DoTask",
						DoTaskArgs{
							JobName:       jobName,
							File:          file,
							Phase:         phase,
							TaskNumber:    tid,
							NumOtherPhase: n_other,
						}, nil)
					if success == true {
						taskDone()
						wg.Done()
						registerChan <- workerAddr
					} else {
						tasks <- tid
						wg.Done()
					}
				}(waddr, taskID)
			}
		}
	}()
	wg.Wait()
}
